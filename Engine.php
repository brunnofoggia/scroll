<?php

namespace Scroll;

/**
 * Engine
 *
 * A toolbox/interface to FileSystem / Database Engines
 *
 * @package     Scroll
 * @category	Utility
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/scroll
 */
trait Engine {

    use \DarkTrait;

    /**
     * Default attributes
     * @var array
     */
    protected $basAttrDefaults = [
        'language' => 'en',
        // used to split filename into sections [date, lang, name]
        'docsNameFlagSeparator' => '_',
        // sections used on filename sorted
        'docsNameMap' => ['date', 'language', 'name'],
        // extensions mapped
        'docsExt' => ['md', 'html', 'php'],
        // path for files
        'docsPath' => '.',
        // post columns
        'column.post.title' => 'title',
        'column.post.name' => 'name',
        'column.post.created' => 'created',
    ];

    /**
     * Merge own default attributes with childs
     * @return array
     */
    public function getAttrProperty() {
        return array_merge($this->basAttrDefaults, $this->attrDefaults);
    }

    /**
     * Import attribute list
     * @param type $attrList
     */
    public function __construct($attrList) {
        $this->setAttrList($attrList);
    }

    /**
     * Create URL to open cover url
     * @param array $item
     * @return string
     */
    public function createUrl($item) {
        $nameMap = $this->getAttr('docsNameMap');
        $pieces = [];

        array_search('date', $nameMap) !== false && $pieces[] = date('Y/m/d', strtotime($item[$this->getAttr('column.post.created')]));
        array_search('name', $nameMap) !== false && $pieces[] = $item[$this->getAttr('column.post.name')];

        return implode('/', $pieces);
    }

    /**
     * Get a list of post main columns and their custom names
     * @return array
     */
    public function getColumnList() {
        return [
            'name' => $this->getAttr('column.post.name'),
            'title' => $this->getAttr('column.post.title'),
            'created' => $this->getAttr('column.post.created'),
        ];
    }

    /**
     * Capture docs path
     * @return string
     */
    public function getDocsPath() {
        $docsPath = empty($this->getAttr('docsPath')) ? $this->basAttrDefaults['docsPath'] : $this->getAttr('docsPath');
        substr($docsPath, -1) !== '/' && ($docsPath .= '/');

        return $docsPath;
    }

    /**
     * Get a list of all files
     * @access public
     */
    abstract public function getListNames($page = 1, $limit = NULL);

    /**
     * Get a list of files and it's contents
     * @access public
     */
    abstract public function getList($page = 1, $limit = NULL);

    /**
     * Get an item from it's path
     * @access public
     */
    abstract public function getItem();

    /**
     * Find files by name
     * @param string $name
     * @param int $page
     * @param int $limit
     * @access public
     */
    abstract public function findByName($name, $page = 1, $limit = NULL);

    /**
     * Get a file by it's name
     * @param string $name
     * @param int $page
     * @param int $limit
     * @access public
     */
    abstract public function getByName($name);

    /**
     * Find files by date
     * @param string $date
     * @param int $page
     * @param int $limit
     * @access public
     */
    abstract public function findByDate($date, $page = 1, $limit = NULL);

    /**
     * Find a file by name or date
     * @param string $content
     * @param int $page
     * @param int $limit
     * @access public
     */
    abstract public function find($content, $page = 1, $limit = NULL);
}
