<?php

namespace Scroll;

use \Michelf\Markdown;

/**
 * Database Engine
 *
 * Provide a way to map records and its files according to attributes set
 *
 * @package     Scroll
 * @category	Utility
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/scroll
 */
define('Scroll\Database\NoMagic', 0);
define('Scroll\Database\MagicEnd', 1);
define('Scroll\Database\MagicBoth', 2);

class Database {

    use Engine;

    /**
     * Default attributes
     * @var array
     */
    protected $attrDefaults = [
        'findMagic' => Database\MagicBoth,
        'db.config' => 'default',
        'table.post' => 'post',
        'column.post.id' => 'id',
    ];
    protected static $postTable;

    /**
     * Estabilish database connection on Whalen component and load posts table
     * @param array $attrList
     */
    public function __construct($attrList) {
        $this->setAttrList($attrList);
        $dbSettings = [
            'host' => $this->getAttr('db.host'),
            'user' => $this->getAttr('db.user'),
            'pass' => $this->getAttr('db.pass'),
            'dbname' => $this->getAttr('db.name')
        ];

        \doctrine\Whalen::setConnConfig($this->getAttr('db.config'), $dbSettings);
        static::$postTable = \doctrine\Whalen::load($this->getAttr('table.post'));
    }

    /**
     * Get a list of all files
     * @param int $page
     * @param int $limit
     * @return array list of files found
     */
    public function getListNames($page = 1, $limit = NULL) {
        return $this->filterDocsNamesByName(NULL, $page, $limit);
    }

    /**
     * Get a list of files filtering them by date
     * @param string $date [format yyyy, yyyymm or yyyymmdd]
     * @return array list of files found
     */
    public function filterDocsNamesByDate($date = '', $page = 1, $limit = NULL) {
        empty($page) && ($page = 1);
        (string) $limit === '' && ($limit = $this->getAttr('listLimit'));
        $criteria = [$this->getAttr('column.post.created') . " LIKE '" . $date . "%'"];
        $results = $this->prepareItems(static::$postTable->find($criteria, $limit, $page - 1));

        return $results;
    }

    /**
     * Get a list of files filtering them by name
     * @param string $string
     * @return array list of files found
     */
    public function filterDocsNamesByName($name = '', $findMagic = NULL, $page = 1, $limit = NULL) {
        empty($page) && ($page = 1);
        (string) $limit === '' && ($limit = $this->getAttr('listLimit'));
        $findMagic === NULL && ($findMagic = $this->getAttr('findMagic'));
        $criteria = [];

        $magicBefore = !empty($findMagic) && $findMagic === Database\MagicBoth ? '%' : '';
        $magicAfter = !empty($findMagic) ? '%' : '';
        $name !== NULL && ($criteria = [$this->getAttr('column.post.title') . " LIKE '" . $magicBefore . $name . $magicAfter . "'" . ' OR ' .
            $this->getAttr('column.post.name') . " LIKE '" . $magicBefore . $name . $magicAfter . "'"]);

        $results = $this->prepareItems(static::$postTable->find($criteria, $limit, $page - 1));

        return $results;
    }

    protected function prepareItems($list) {
        foreach ($list as $x => $item) {
            $list[$x] = $this->prepareItem($item);
        }
        return $list;
    }

    protected function prepareItem($item) {
        $item[$this->getAttr('column.post.created')] = str_replace('-', '', explode(' ', $item[$this->getAttr('column.post.created')], 2)[0]);
        $item['url'] = $this->createUrl($item);

        return $item;
    }

    /**
     * Get a list of files and it's contents
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getList($page = 1, $limit = NULL) {
        empty($page) && ($page = 1);
        (string) $limit === '' && ($limit = $this->getAttr('listLimit'));
        $list = $this->prepareItems($this->getListNames($page, $limit));

        return $list;
    }

    /**
     * Get an item from it's path
     * @param string $path
     * @return array item data [name, created date, language, content]
     */
    public function getItem($item) {
        $flagSeparator = $this->getAttr('docsNameFlagSeparator');
        $lang = $this->getAttr('language');
        $defaultLang = $this->getAttr('language', true);

        $post = [];

        while (empty($path)) {
            $filenamePiece = $item[$this->getAttr('column.post.created')] . $flagSeparator . $lang . $flagSeparator . $item[$this->getAttr('column.post.name')];
            $path = @glob($this->getDocsPath() . $filenamePiece . '.{' . implode(',', $this->getAttr('docsExt')) . '}', GLOB_BRACE)[0];

            if (empty($path)) {
                $lang = $defaultLang;
            } else {
                $pathPieces = explode('/', $path);
                $filename = $pathPieces[count($pathPieces) - 1];
                $filenamePiece = NULL;
            }
        }

        list($post['date'], $post['lang'], $filenamePiece) = explode($flagSeparator, $filename);

        preg_match('/\.(' . implode('|', $this->getAttr('docsExt')) . ')$/', $filenamePiece, $ext) && ($ext = $ext[1]);
        $post['title'] = $item[$this->getAttr('column.post.title')];
        $post['name'] = $item[$this->getAttr('column.post.name')];

        $post['body'] = file_get_contents($path);
        if ($ext === 'md') {
            $contentHtml = Markdown::defaultTransform($post['body']);
        }

        return $post;
    }

    /**
     * Find files by date
     * @param string $date [format yyyy, yyyymm or yyyymmdd]
     * @param int $page 
     * @param int $limit
     * @return array
     */
    public function findByDate($date, $page = 1, $limit = NULL) {
        empty($page) && ($page = 1);

        $results = [];

        if (!is_array($date)) {
            preg_match_all('/^(\d{4})(\d{2})?(\d{2})?$/', $date, $matches, PREG_SET_ORDER);
            count($matches[0]) < 4 && ($matches[0] = $matches[0] + (array) array_fill(count($matches[0]), 4 - count($matches[0]), ''));
            $date = [];
            list($date['fulldate'], $date['year'], $date['month'], $date['day']) = $matches[0];
        }

        $fullDate = $date['year'] . (empty($date['month']) ? '' : '-' . $date['month'] . (empty($date['day']) ? '' : '-' . $date['day']));
        foreach ($this->filterDocsNamesByDate($fullDate, $page, $limit) as $x => $item) {
            $results[] = $item;
        }

        return $results;
    }

    /**
     * Find files by name
     * @param string $name
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function findByName($name, $page = 1, $limit = NULL) {
        empty($page) && ($page = 1);

        $results = [];
        foreach ($this->filterDocsNamesByName($name, NULL, $page, $limit) as $x => $item) {
            $results[] = $item;
        }

        return $results;
    }

    /**
     * Get a file by it's name
     * @param string $name
     * @return array
     */
    public function getByName($name) {
        $results = [];
        foreach ($this->filterDocsNamesByName($name, Database\NoMagic) as $x => $itemData) {
            $item = $this->getItem($itemData);
            return $item;
        }
        return [];
    }

    /**
     * Find a file by name or date
     * @param string $content
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function find($content, $page = 1, $limit = NULL) {
        empty($page) && ($page = 1);
        $content = trim($content);
        $date = NULL;

        preg_match('/^\d{4}(\-\d{2}(\-\d{2})?)?$/', $content) && ($date = $content);
        preg_match('/^\d{4}(\-\d{2}(\-\d{2})?)?$/', $content) && ($date = str_replace('-', '', $content));
        preg_match('/^((\d{2}\/\d{2}\/\d{4})|((\d{2}\/)?\d{4}))$/', $content) && ($date = implode('', array_reverse(explode('/', $content))));

        if (!empty($date)) {
            $results = $this->findByDate($date);
        } else {
            $results = $this->findByName($content);
        }

        return $results;
    }

}
