<?php

namespace Scroll;

use \Michelf\Markdown;

/**
 * FileSystem Engine
 *
 * Provide a way to map files according to attributes set
 *
 * @package     Scroll
 * @category	Utility
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/scroll
 */
class FileSystem {

    use Engine;

    protected $attrDefaults = [];

    /**
     * Locate files according to what's asked
     * @param string $string
     * @param string $date [format yyyymmdd]
     * @param string $language
     * @param string $ext [possible extensions split by ,]
     * @param string $path 
     * @return array list of files found
     */
    public function locateFiles($string, $date, $language, $ext, $path = NULL) {
        $flagSeparator = $this->getAttr('docsNameFlagSeparator');
        $nameMap = $this->getAttr('docsNameMap');

        $path === NULL && ($path = $this->getDocsPath());

        array_search('language', $nameMap) !== false && ($languagePiece = (!empty($language) ? $language : '*') . $flagSeparator);
        array_search('date', $nameMap) !== false && ($datePiece = (!empty($date) ? $date : '*') . $flagSeparator);

        $globStr = $path . @$datePiece . @$languagePiece . $string . '.{' . $ext . '}';
        return glob($globStr, GLOB_BRACE);
    }

    /**
     * Get a list of all files
     * @param int $page
     * @param int $limit
     * @return array list of files found
     */
    public function getListNames($page = 1, $limit = NULL) {
        $string = '*';
        $language = $this->getAttr('language');
        $date = '*';
        $ext = implode(',', $this->getAttr('docsExt'));

        $list = array_reverse($this->locateFiles($string, $date, $language, $ext));
        return $list;
    }

    /**
     * Get a list of files filtering them by date
     * @param string $date [format yyyy, yyyymm or yyyymmdd]
     * @return array list of files found
     */
    public function filterDocsNamesByDate($date = '') {
        $string = '*';
        $language = $this->getAttr('language');
        strlen($date) < 8 && ($date .= '*');
        $ext = implode(',', $this->getAttr('docsExt'));

        $list = array_reverse($this->locateFiles($string, $date, $language, $ext));
        return $list;
    }

    /**
     * Get a list of files filtering them by name
     * @param string $string
     * @return array list of files found
     */
    public function filterDocsNamesByName($string = '') {
        $language = $this->getAttr('language');
        $date = '*';
        $ext = implode(',', $this->getAttr('docsExt'));

        $list = array_reverse($this->locateFiles($string, $date, $language, $ext));
        return $list;
    }

    /**
     * Capture a filename from a fullpath
     * @param string $path
     * @return string filename
     */
    public function getItemName($path) {
        $docsPath = $this->getDocsPath();

        $name = substr($path, strpos($path, $docsPath) + strlen($docsPath));
        return $name;
    }

    /**
     * Get a list of files and it's contents
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getList($page = 1, $limit = NULL) {
        empty($page) && ($page = 1);
        (string) $limit === '' && ($limit = $this->getAttr('listLimit'));
        $docsNames = !empty($limit) ? array_slice($this->getListNames(), ($page - 1) * $limit, $limit) : $this->getListNames();

        $list = [];
        foreach ($docsNames as $path) {
            $list[] = $this->getItem($path);
        }

        return $list;
    }

    /**
     * Get an item from it's path
     * @param string $path
     * @return array item data [name, created date, language, content]
     */
    public function getItem($path) {
        $flagSeparator = $this->getAttr('docsNameFlagSeparator');

        $post = [];

        // Extract the date
        $filename = $this->getItemName($path);
        $filenameMap = explode($flagSeparator, $filename);
        $nameMap = $this->getAttr('docsNameMap');

        $post['created'] = ($pos = array_search('date', $nameMap)) !== false ? $filenameMap[$pos] : '';
        $post['lang'] = ($pos = array_search('language', $nameMap)) !== false ? $filenameMap[$pos] : '';
        ($pos = array_search('name', $nameMap)) !== false && ($filenamePiece = $filenameMap[$pos]);

        // The post URL
        preg_match('/\.(' . implode('|', $this->getAttr('docsExt')) . ')$/', $filenamePiece, $ext) && ($ext = $ext[1]);
        $post['title'] = $post['name'] = preg_replace('/\.' . $ext . '$/', '', $filenamePiece);

        $post['body'] = file_get_contents($path);
        if ($ext === 'md') {
            // Get the contents and convert it to HTML
            $contentHtml = Markdown::defaultTransform($post['body']);

            // Extract the title and body
            $md = explode('</h1>', $contentHtml, 2);

            if (count($md) < 2) {
                $post['body'] = $md[0];
            } else {
                list($post['title'], $post['body']) = $md;
                $post['title'] = trim(str_replace('<h1>', '', $post['title']));
            }
        }

        $post['url'] = $this->createUrl($post);

        return $post;
    }

    /**
     * Find files by date
     * @param string $date [format yyyy, yyyymm or yyyymmdd]
     * @param int $page 
     * @param int $limit
     * @return array
     */
    public function findByDate($date, $page = 1, $limit = NULL) {
        empty($page) && ($page = 1);

        $results = [];

        if (!is_array($date)) {
            preg_match_all('/^(\d{4})(\d{2})?(\d{2})?$/', $date, $matches, PREG_SET_ORDER);
            count($matches[0]) < 4 && ($matches[0] = $matches[0] + (array) array_fill(count($matches[0]), 4 - count($matches[0]), ''));
            $date = [];
            list($date['fulldate'], $date['year'], $date['month'], $date['day']) = $matches[0];
        }

        foreach ($this->filterDocsNamesByDate($date['year'] . $date['month'] . $date['day']) as $x => $path) {
            $results[] = $this->getItem($path);
        }

        (string) $limit === '' && ($limit = $this->getAttr('listLimit'));
        $results = !empty($limit) ? array_slice($results, ($page - 1) * $limit, $limit) : $results;

        return $results;
    }

    /**
     * Find files by name
     * @param string $name
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function findByName($name, $page = 1, $limit = NULL) {
        empty($page) && ($page = 1);

        $results = [];
        foreach ($this->getListNames() as $x => $path) {
            $docInfo = $this->getItem($path);
            if (strpos($docInfo['name'], $name) !== false) {
                $results[] = $this->getItem($path);
            }
        }

        (string) $limit === '' && ($limit = $this->getAttr('listLimit'));
        $results = !empty($limit) ? array_slice($results, ($page - 1) * $limit, $limit) : $results;

        return $results;
    }

    /**
     * Get a file by it's name
     * @param string $name
     * @return array
     */
    public function getByName($name) {

        $results = [];
        foreach ($this->filterDocsNamesByName($name) as $x => $path) {
            $item = $this->getItem($path);
            if (strpos($item['name'], $name) !== false) {
                return $item;
            }
        }
        return [];
    }

    /**
     * Find a file by name or date
     * @param string $content
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function find($content, $page = 1, $limit = NULL) {
        empty($page) && ($page = 1);
        $content = trim($content);
        $date = NULL;

        preg_match('/^\d{4}(\-\d{2}(\-\d{2})?)?$/', $content) && ($date = $content);
        preg_match('/^\d{4}(\-\d{2}(\-\d{2})?)?$/', $content) && ($date = str_replace('-', '', $content));
        preg_match('/^((\d{2}\/\d{2}\/\d{4})|((\d{2}\/)?\d{4}))$/', $content) && ($date = implode('', array_reverse(explode('/', $content))));

        if (!empty($date)) {
            $results = $this->findByDate($date);
        } else {
            $results = $this->findByName($content);
        }

        return $results;
    }

}
