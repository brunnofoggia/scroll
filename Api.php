<?php

namespace Scroll;

/**
 * Api
 *
 * A collection of routes to consume mapped content over API
 *
 * @package     Scroll
 * @category	Utility
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/scroll
 */
trait Api {

    use \DarkTrait;

    /**
     * Attribute default values
     * @var array
     */
    protected $apiAttrDefaults = [
        'route.getList' => 'getList',
        'route.find' => 'find',
        'route.findByName' => 'findByName',
        'route.getByName' => 'getByName',
        'route.findByDate' => 'findByDate',
    ];

    /**
     * Get attribute default values
     * @return array
     */
    public function getAttrProperty() {
        return isset($this->apiAttrDefaults) ? $this->apiAttrDefaults : [];
    }

    public function getList($args) {
        header('Content-type: application/json');
        die(json_encode($this->engineInstance->getList(@$args['page'], @$args['limit'])));
    }

    public function findByDate($args) {
        header('Content-type: application/json');
        $date = [
            'year' => (string) @$args['year'],
            'month' => (string) @$args['month'],
            'day' => (string) @$args['day']
        ];
        die(json_encode($this->engineInstance->findByDate($date, @$args['page'], @$args['limit'])));
    }

    public function findByName($args) {
        header('Content-type: application/json');
        die(json_encode($this->engineInstance->findByName($args['name'], @$args['page'], @$args['limit'])));
    }

    public function getByName($args) {
        header('Content-type: application/json');
        die(json_encode($this->engineInstance->getByName((string) @$args['name']), JSON_FORCE_OBJECT));
    }

    public function find($args) {
        header('Content-type: application/json');
        die(json_encode($this->engineInstance->find($args['content'], @$args['page'], @$args['limit'])));
    }

    /**
     * Apply Api routes to the application
     * @param object $app slim app instance
     */
    abstract public function applyApiRoutes($app);

}
