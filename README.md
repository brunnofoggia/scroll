# Scroll

[![Minimum PHP Version](http://img.shields.io/badge/php-%3E%3D%205.4-8892BF.svg)](https://php.net/)
[![License](https://img.shields.io/badge/license-MIT-4C9182.svg)](https://opensource.org/licenses/MIT)

This is a toolkit to build documentations, journals, blogs much flexible than if you use another tool, 
because it's focused on the main need, to show stored content, customize it visually, search between files.

## Version notice

Scroll comes with **Slim 3.x** Engine ready to be used, but you can extend its functionalities for another framework

## What its provided?

### By configuration
- Define default dir for files that can be viewed
- Customize extensions
- Customize filename pattern
- Set a layout file as master page, to receive content rendered
- Choose between FileSystem and Database Engine

### By Engines
- FileSystem Engine allows you to publish your files and organize their names splitting than into date, language and name
- DataBase Engine allows you that same as FileSystem, but all your files will have a record with their attributes. This allows you to have more attributes for every file, like tags.



## Installation

A initial composer.json would looks like:

    {
      "require": {
          "scroll": "1.*"
      }
    }

## Usage

### index.php

There are two thins that can be customized here. Vendor directory and GMT.

    <?php
    require "vendor/autoload.php";  
    
    // Customize your GMT
    date_default_timezone_set("America/Sao_Paulo");
    
    $app = new \Slim\App([]);
    
    $config = parse_ini_file("config.ini", true);
    $test = new \Scroll\Slim\Core($config, $app);
    
    $app->run();
    ?>

### layout

To receive the content loaded into the layout you just have to add the following code wherever you want to display it

    <?=$content?>

### config.ini

;Config ini could have the settings listed bellow. None of them are required (they have defaults).  
;- Dont forget to uncomment lines you will set by removing ";" at their beginning

;homePage = 'readme'  
; File loaded for route "/" - homepage

;layout = "layout.php"  
; Master Page - Layout to all your pages (leave blank if you dont want to have it)

;docsPath = "files/"  
; Path where files will be located at

;language = "en"  
; Default language - in case you want to have your documents on more than one language

;engine = "file"  
; Can be set as "file" or "db"


; If engine was set as "db" the settings bellow are required.
[Click here to read about db structure](db.md)

;[db]  
 ;name = "teste"  
 ;host = "localhost"  
 ;user = "root"  
 ;pass = "12345678"  


; Define the pieces of your pages names.

;[docsNameMap]

; As an example, you can compose the filename with only date and name. It would be like  
;0 = date  
;1 = name  

; or you can have date, language and name  
;0 = date  
;1 = language  
;2 = name  

; The extensions your pages could have. You can use markdown by enabling "md" line bellow, or just html/php.  
;[docsExt]  
;0 = md  
;1 = php  
;2 = html  

#### config.ini samples

- [documentation](docs/config.ini.documentation.md)  
- [personal blog without database](docs/config.ini.blog.file.md)  
- [personal blog with database](docs/config.ini.blog.db.md)  
- [multilingual journal with database](docs/config.ini.journal.db.md)  

### Routes

#### Web

- "/" - view home page
- "/view/pagename" - display a page by it's name, if the app its db based then the page should be referenced on post table 
- "/page/pagename" - display a page by it's name

#### JSON Api

- "/api/findByDate/yyyy/mm/dd", "/api/findByDate/yyyy/mm" or "/api/findByDate/yyyy" - return a set of results for the given date
- "/api/findByName/name" - return a set of results for the given name
- "/api/find/something" - "something" could by a part of a name or a date (yyyy-mm-dd)