<?php

namespace Scroll;

/**
 * Scroll Core
 *
 * Integrate Api and Navigation Methods to access all the content stored and mapped according to their settings
 * 
 * @package     Scroll
 * @category	Utility
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/scroll
 */
trait Core {

    use \Scroll\Api,
        \Scroll\Cover {

        \Scroll\Api::formatAttrName insteadof \Scroll\Cover;
        \Scroll\Api::getAttr insteadof \Scroll\Cover;
        \Scroll\Api::setAttrList insteadof \Scroll\Cover;
        
        \Scroll\Api::formatAttrName as SA_formatAttrName;
        \Scroll\Api::getAttr as SA_getAttr;
        \Scroll\Api::getAttrProperty as SA_getAttrProperty;

        \Scroll\Cover::formatAttrName as CO_formatAttrName;
        \Scroll\Cover::getAttr as CO_getAttr;
        \Scroll\Cover::getAttrProperty as CO_getAttrProperty;
    }
    /**
     * Default attributes
     * @var array
     */
    protected $attrDefaults = [
        'engineList' => [
            'file' => 'FileSystem',
            'db' => 'Database'
        ],
        'defaultEngine' => 'file'
    ];
    
    /**
     * Merge own default attributes with api and cover attributes
     * @return array
     */
    public function getAttrProperty() {
        $data = array_merge_recursive($this->SA_getAttrProperty(), $this->CO_getAttrProperty(), $this->attrDefaults);
        return $data;
    }

    /**
     * Import attribute list, Instantiate Engine, apply routes
     * @param array $attrList
     * @param object $app Application
     */
    public function __construct(Array $attrList, $app) {
        $this->setAttrList($attrList);
        empty($this->getAttr('engine')) && ($this->engine = $this->getAttr('defaultEngine'));
        $engine = '\\' . explode('\\', __NAMESPACE__)[0] . '\\' . $this->getAttr('engineList')[$this->getAttr('engine')];

        $this->engineInstance = new $engine($attrList);

        $this->applyApiRoutes($app);
        $this->applyCoverRoutes($app);
    }
    
    /**
     * Set engine language attribute and save it
     * @param type $lang
     */
    public function setLanguage($lang = NULL) {
        @session_start();
        if(empty($lang)) {
            if(!empty($_SESSION['scroll.lang'])) {
                $lang = $_SESSION['scroll.lang'];
            } else {
                $lang = explode(',',explode(';', $_SERVER['HTTP_ACCEPT_LANGUAGE'])[0])[1];
            }
        }
        
        $this->engineInstance->language = $lang;
        $_SESSION['scroll.lang'] = $lang;
    }

}
