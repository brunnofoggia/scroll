<?php

namespace Scroll\Slim;

/**
 * Slim Core
 *
 * Implementation of Scroll for Slim 3
 * 
 * Apply slim routes to consume mapped content over API and to navigate through scroll mapped content
 * 
 * @package     Scroll
 * @category	Utility
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/scroll
 */
class Core {

    use \Scroll\Core;
    
    /**
     * Apply Cover routes to slim app
     * @param object $app slim app instance
     */
    public function applyCoverRoutes($app) {
        $self = $this;
        !empty($this->getAttr('route.openFileByName')) && $app->map(['GET'], '/' . $this->getAttr('route.openFileByName') . '/{year:[0-9]+}/{month:[0-9]+}/{day:[0-9]+}/{name}', function ($request, $response, $args) use ($app, $self) {
                    $self->openFileByName($args, $request->getUri()->getBaseUrl());
                });
        !empty($this->getAttr('route.openFileByName')) && $app->map(['GET'], '/' . $this->getAttr('route.openFileByName') . '/{lang}/{year:[0-9]+}/{month:[0-9]+}/{day:[0-9]+}/{name}', function ($request, $response, $args) use ($app, $self) {
                    $self->openFileByName($args, $request->getUri()->getBaseUrl());
                });
        !empty($this->getAttr('route.openFileByName')) && $app->map(['GET'], '/' . $this->getAttr('route.openFileByName') . '/{name}', function ($request, $response, $args) use ($app, $self) {
                    $self->openFileByName($args, $request->getUri()->getBaseUrl());
                })->setName('openFileByName');
        !empty($this->getAttr('route.openFileByName')) && $app->map(['GET'], '/' . $this->getAttr('route.openFileByName') . '/{lang}/{name}', function ($request, $response, $args) use ($app, $self) {
                    $self->openFileByName($args, $request->getUri()->getBaseUrl());
                })->setName('openFileByName');
        !empty($this->getAttr('route.search')) && $app->map(['GET'], '/' . $this->getAttr('route.search') . '/{content}[/{page:[0-9]+}[/{limit:[0-9]+}]]', function ($request, $response, $args) use ($app, $self) {
                    $self->search($args, $request->getUri()->getBaseUrl());
                })->setName('openFileByName');
        !empty($this->getAttr('route.openPageByName')) && $app->map(['GET'], '/' . $this->getAttr('route.openPageByName') . '/{name}', function ($request, $response, $args) use ($app, $self) {
                    $self->openPageByName($args, $request->getUri()->getBaseUrl());
                })->setName('openPageByName');
        !empty($this->getAttr('route.openPageByName')) && $app->map(['GET'], '/' . $this->getAttr('route.openPageByName') . '/{lang}/{name}', function ($request, $response, $args) use ($app, $self) {
                    $self->openPageByName($args, $request->getUri()->getBaseUrl());
                })->setName('openPageByName');
        !empty($this->getAttr('homePage')) && $app->map(['GET'], '/', function ($request, $response, $args) use ($app, $self) {
                    $self->home($args, $request->getUri()->getBaseUrl());
                });
        !empty($this->getAttr('homePage')) && $app->map(['GET'], '/{lang}/', function ($request, $response, $args) use ($app, $self) {
                    $self->home($args, $request->getUri()->getBaseUrl());
                });
    }
    
    /**
     * Apply Api routes to slim app
     * @param object $app slim app instance
     */
    public function applyApiRoutes($app) {
        $self = $this;
        $preffix = '/api';

        !empty($this->getAttr('route.getList')) && $app->map(['GET'], $preffix . '/' . $this->getAttr('route.getList') . '[/{page:[0-9]+}[/{limit:[0-9]+}]]', function ($request, $response, $args) use ($app, $self) {
                    $self->getList($args);
                });
        !empty($this->getAttr('route.findByDate')) && $app->map(['GET'], $preffix . '/' . $this->getAttr('route.findByDate') . '[/{year:[0-9]+}[/{month:[0-9]+}[/{day:[0-9]+}[/{page:[0-9]+}[/{limit:[0-9]+}]]]]]', function ($request, $response, $args) use ($app, $self) {
                    $self->findByDate($args);
                });
        !empty($this->getAttr('route.findByName')) && $app->map(['GET'], $preffix . '/' . $this->getAttr('route.findByName') . '/{name}[/{page:[0-9]+}[/{limit:[0-9]+}]]', function ($request, $response, $args) use ($app, $self) {
                    $self->findByName($args);
                });
        !empty($this->getAttr('route.getByName')) && $app->map(['GET'], $preffix . '/' . $this->getAttr('route.getByName') . '/{name}', function ($request, $response, $args) use ($app, $self) {
                    $self->getByName($args);
                });
        !empty($this->getAttr('route.find')) && $app->map(['GET'], $preffix . '/' . $this->getAttr('route.find') . '/{content}[/{page:[0-9]+}[/{limit:[0-9]+}]]', function ($request, $response, $args) use ($app, $self) {
                    $self->find($args);
                });
    }

}
