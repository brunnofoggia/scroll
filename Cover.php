<?php

namespace Scroll;

/**
 * Cover
 *
 * A collection of routes to navigate through scroll mapped content
 *
 * @package     Scroll
 * @category	Utility
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/scroll
 */
trait Cover {

    use \DarkTrait,
        \ChillRender {
        \DarkTrait::getAttr insteadof \ChillRender;
        \DarkTrait::formatAttrName insteadof \ChillRender;
        \DarkTrait::setAttrList insteadof \ChillRender;
    }

    /**
     * Attribute default values
     * @var array
     */
    protected $coverAttrDefaults = [
        'route.openFileByName' => 'view',
        'route.openPageByName' => 'page',
        'route.search' => 'search',
        'homePage' => 'home',
        'homePageLayout' => true,
        'searchPage' => 'search',
        'searchPageLayout' => true,
    ];

    /**
     * Get attribute default values
     * @return array
     */
    public function getAttrProperty() {
        return isset($this->coverAttrDefaults) ? $this->coverAttrDefaults : [];
    }
    
    public function home($args, $baseurl) {
        $this->setLanguage(@$args['lang']);
        $this->openPageByName(['name' => $this->getAttr('homePage')], $baseurl);
    }

    public function openFileByName($args, $baseurl) {
        $this->setLanguage(@$args['lang']);
        $item = $this->engineInstance->getByName($args['name']);
        if ($item) {
            $content = $this->renderViewLayout(['content' => $item['body'], 'item' => $item]);
            if ($content !== false) {
                echo $content;
                return;
            }
            echo ($item['body']);
            return;
        }

        echo '404';
    }

    public function openPageByName($args, $baseurl) {
        $this->setLanguage(@$args['lang']);
        $content = $this->renderView($args['name'], [], $this->getAttr('homePageLayout'));
        if ($content !== false) {
            echo $content;
            return;
        }

        $this->openFileByName($args, $baseurl);
    }

    public function search($args, $baseurl) {
        empty($args['page']) && ($args['page'] = 1);
        $results = ($this->engineInstance->find($args['content'], $args['page'], @$args['limit']));

        $data = [
            'results' => $results,
            'route' => [
                'base' => $baseurl,
                'openFileByName' => $this->getAttr('route.openFileByName'),
            ],
            'column' => $this->engineInstance->getColumnList()
        ];

        $this->renderView($this->getAttr('searchPage'), $data, $this->getAttr('searchPageLayout'));
    }

    /**
     * Apply Cover routes to the application
     * @param object $app slim app instance
     */
    abstract public function applyCoverRoutes($app);

}
